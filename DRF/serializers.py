from rest_framework import serializers
from .models import GuruTugas, Tempat

class GuruTugasSerializer(serializers.ModelSerializer):
    tempat = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta:
        model = GuruTugas
        fields = ["id", "nama", "gurutugas"]

class TempatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tempat
        fields = ["id", "nama", "tempat"]