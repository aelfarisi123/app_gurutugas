from django.shortcuts import render
from rest_framework import status, permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.http import Http404
from rest_framework.views import APIView
from .models import GuruTugas, Tempat
from .serializers import GuruTugasSerializer, TempatSerializer


@api_view(['GET', 'POST'])
@permission_classes([permissions.AllowAny])
def GuruTugas_list(request, format=None):

    if request.method == 'GET':
        GuruTugas =GuruTugas .objects.all()
        serializer = GuruTugasSerializer(GuruTugas, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = TempatSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def gurutugas_detail(request, pk, format=None):
   
    try:
        gurutugas = GuruTugas.objects.get(pk=pk)
    except GuruTugas.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = GuruTugasSerializer(gurutugas)
        return Response(serializer.data)
     elif request.method == 'PUT':
        serializer = GuruTugasugasSerializer(gurutugas, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        gurutugas.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TempatList(APIView):
    
    permission_classes = [permissions.AllowAny]
    def get(self, request, format=None):
        produk = Tempat.objects.all()
        serializer = TempatSerializer(produk, many=True)
        return Response(serializer.data)
    
    def post(self, request, format=None):
        serializer = TampatSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class TampatDetail(APIView):
  
    parser_classes = [permissions.AllowAny]
    def get_object(self, pk):
        try:
            return Tampat.objects.get(pk=pk)
        except Tampat.DoesNotExist:
            raise Http404
        
    def get(self, request, pk, format=None):
        produk = self.get_object(pk)
        serializer = TempatSerializer(tempat)
        return Response(serializer.data)
    
    def put(self, request, pk, format=None):     
        produk = self.get_object(pk)
        serializer = TempatSerializer(tempat, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, request, pk, format=None):
        produk = self.get_object(pk=pk)
        produk.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)