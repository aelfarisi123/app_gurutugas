
from django.db import models

class GuruTugas(models.Model):
    nama = models.CharField(max_length=255)

    def __str__(self):
        return self.nama

class Tempat (models.Model):
    GuruTugas= models.ForeignKey(GuruTugas, related_name="tempat", on_delete=models.CASCADE)
    nama = models.CharField(max_length=200)

    def __str__(self):
        return self.nama
